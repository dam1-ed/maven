import java.util.Scanner;

public class examen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		Integer selector = 0,size_trian,contador;
		Float numero,total,media;
		String dato,palabra;
		
		do {
			
			//Select option
			System.out.print("MENU\n==========\n1. Palabra invertida\n2. Tri�ngulo n�meros\n3. Suma/Media\n4. Salir\n\nIntroduce una opci�n: ");
			dato = scan.nextLine();
			selector = Integer.parseInt(dato);
		
			switch (selector) {
			case 1: 
				System.out.print("Introduce una palabra: ");
				palabra = scan.nextLine();
				
				for (int i = 0;i < palabra.length();i++) {System.out.print(palabra.toUpperCase().charAt(palabra.length()-1-i));}
				System.out.println();
				
				break;
			case 2: 
				do {
					System.out.print("Introduzca el tama�o del lado del tri�ngulo: ");
					dato = scan.nextLine();
					size_trian = Integer.parseInt(dato);
					if (size_trian < 1) {System.out.println("Introduzca un n�mero positivo");}
				} while (size_trian < 1);
				
				for (int i = size_trian;i > 0;i--) {
					for (int x = 0;x < i;x++) {System.out.print(x+1+" ");}
					System.out.println();
				}
				
				break;
			case 3: 
				total = 0f;
				contador = 0;
				do {
					System.out.print("Introduce un n�mero (Negativo para salir): ");
					dato = scan.nextLine();
					numero = Float.parseFloat(dato);
					
					if (numero < 0) {break;}
					else {total += numero;contador += 1;}
				} while (numero > 0);
				
				if (contador == 0) {
					System.out.println("No has introducido ning�n n�mero v�lido");
				}
				else {
					media = total/contador;
				
					System.out.println("La suma de los n�meros es "+total);
					System.out.println("La media de los n�meros es "+media);
					}
				
				break;
			case 4:
				System.out.println("_________\nSaliendo...");
				break;
			default:
				System.out.println("-----------------\nOpci�n incorrecta\n-----------------");
			}
			
		} while (selector != 4);
		
		scan.close();
	}

}
